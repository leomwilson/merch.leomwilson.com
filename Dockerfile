FROM subfuzion/netcat:latest

COPY response.http /var/www/response.http
COPY serve.sh /var/www/serve.sh

ENTRYPOINT /var/www/serve.sh
